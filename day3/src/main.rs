use std::{fs, path::PathBuf, collections::HashMap, collections::HashSet};

fn gen_alpha_map() -> HashMap<char, i32> {

    let mut alpha_low = (b'a'..=b'z')                               // Start as u8
        .filter_map(|c| {
            let c = c as char;                             // Convert to char
            if c.is_alphabetic() { Some(c) } else { None } // Filter only alphabetic chars
        })          
        .collect::<Vec<_>>();
    let alpha_up= (b'A'..=b'Z')                               // Start as u8
        .filter_map(|c| {
            let c = c as char;                             // Convert to char
            if c.is_alphabetic() { Some(c) } else { None } // Filter only alphabetic chars
        })          
        .collect::<Vec<_>>();
    
    alpha_low.extend(alpha_up) ;
    
    let mut point_mapping: HashMap<char, i32> = HashMap::new() ;
     
    let mut it: i32 = 1 ;
    for c in alpha_low {
        point_mapping.insert(c, it) ;
        it+=1;
    }

    return point_mapping ;
}


fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

//    println!("{contents}");

    // part one
    let mut priority_sum: i32 = 0 ;
    let mut matches: Vec<char> = Vec::new() ;
    let alpha_map = gen_alpha_map() ;

    for s in contents.lines() {
        // println!("{s}");
        let half_sack = s.len() / 2 ;
        let left: String  = s.chars().skip(0).take(half_sack).collect() ; 
        let right: String = s.chars().skip(half_sack).take(half_sack).collect() ; 
        let mut left_set: HashSet<char> = HashSet::new() ;
        let mut right_set: HashSet<char> = HashSet::new() ;
        for c in left.chars()  { left_set.insert(c) ; }
        for c in right.chars() { right_set.insert(c) ; }
        for c in left.chars() {
           if right_set.contains(&c) { matches.push(c) ; break ;}
        }
        // println!("{:?}", matches);
    }
    
    // sum matches
    for c in matches {
        priority_sum += alpha_map.get(&c).unwrap() ;
        // print!("{c}") ;
        // println!("{priority_sum}") ;
    }

    println!("{priority_sum}") ;

    // part two
    let mut group_sum: i32 = 0 ;
    let mut matches: Vec<char> = Vec::new() ;

    let mut line0: &str = "" ;
    let mut line1: &str = "" ;
    let mut it = 0 ; 

    for s in contents.lines() {
        if it == 0 { line0 = s ; it += 1 ; continue ; }
        if it == 1 { line1 = s ; it += 1 ; continue ; }
        let line2 = s ;
        let mut set0: HashSet<char> = HashSet::new() ;
        let mut set1: HashSet<char> = HashSet::new() ;
        let mut set2: HashSet<char> = HashSet::new() ;
        for c in line0.chars()  { set0.insert(c) ; }
        for c in line1.chars()  { set1.insert(c) ; }
        for c in line2.chars()  { set2.insert(c) ; }

        let sets: Vec<&HashSet<char>> = Vec::from([&set0,&set1,&set2]) ;
        let intersect = sets
        .iter()
        .fold(None, |acc: Option<HashSet<&char>>, hs| {
            let hs = hs.iter().map(|s| s).collect(); // horrible
            acc.map(|a| a.intersection(&hs).map(|s| *s).collect())
                .or(Some(hs))
        });

        // println!("{line0}");
        // println!("{line1}");
        // println!("{line2}");
        // println!("{:?}", intersect) ;

        for c in intersect.unwrap() {
            matches.push(c.clone());
        }

        it = 0 ; // STATE IS GOOOD
    }

    // println!("{:?}", matches);
    
    // sum matches
   for c in matches {
       group_sum += alpha_map.get(&c).unwrap() ;
    //    print!("{c}") ;
    //    println!("{group_sum}") ;
   }
   
   println!("{group_sum}") ;
    
}