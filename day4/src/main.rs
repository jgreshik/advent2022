use std::{fs,path::PathBuf};

fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

//    println!("{contents}");

    
    let mut overlap_sum: i32 = 0 ; // part one
    let mut overlap_partial_sum: i32 = 0 ; // part two

    for s in contents.lines() {
        // println!("{s}");
        if s.is_empty() {continue;}
        let v: Vec<&str> = s.split(',').collect() ;
        let mut ints: Vec<i32> = Vec::new() ;
        for s in v {
            let j: Vec<&str> = s.split('-').collect() ;
            for i in j { ints.push(i.parse().unwrap()) ; }
        }
        if ( ints[0] >= ints[2] && ints[1] <= ints[3] ) || 
            ( ints[2] >= ints[0] && ints[3] <= ints[1] ) { overlap_sum += 1 ; }
        if !(( ints[0] < ints[2] && ints[1] < ints[2] ) || 
             ( ints[0] > ints[3] && ints[1] > ints[3] )) { overlap_partial_sum += 1 ; }
    }
    
    println!("{overlap_sum}") ;
    println!("{overlap_partial_sum}") ;

}
