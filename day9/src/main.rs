use std::{fs, path::PathBuf, collections::HashMap , collections::HashSet};

// pos1 knot following pos2 knot
fn new_knot_pos (pos1 : (i32 , i32 ), pos2 : (i32 , i32)) -> (i32 , i32) {

    let distance = pos1.0.abs_diff(pos2.0) + pos1.1.abs_diff(pos2.1) ;

    // do not move
    if distance <= 1 || (distance == 2 && pos1.0 != pos2.0 && pos1.1 != pos2.1) {
        return pos1 ;
    }
    // move lateral
    if distance == 2 {
        if pos1.0 < pos2.0 {return (pos1.0 + 1 , pos1.1) ;}
        if pos1.0 > pos2.0 {return (pos1.0 - 1 , pos1.1) ;}
        if pos1.1 < pos2.1 {return (pos1.0     , pos1.1 + 1) ;}
        if pos1.1 > pos2.1 {return (pos1.0     , pos1.1 - 1) ;}
    }
    // move diagonal
    else {
        if pos1.0 < pos2.0 && pos1.1 < pos2.1 {return (pos1.0 + 1 , pos1.1 + 1) ;}
        if pos1.0 < pos2.0 && pos1.1 > pos2.1 {return (pos1.0 + 1 , pos1.1 - 1) ;}
        if pos1.0 > pos2.0 && pos1.1 < pos2.1 {return (pos1.0 - 1 , pos1.1 + 1) ;}
        if pos1.0 > pos2.0 && pos1.1 > pos2.1 {return (pos1.0 - 1 , pos1.1 - 1) ;}
    }

    return pos1 ;

}

fn main() {
    
    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    println!("{}", file_path.display());

    let contents = fs::read_to_string(file_path)
        .expect("file not open");

    // println!("{contents}");

    let decision_map : HashMap<&str, (i32,i32)> = HashMap::from(
        [("L" , (-1,0)),
         ("R" , (1,0)),
         ("U" , (0,1)),
         ("D" , (0,-1))]);

    // part one 

    let mut visited : HashSet<(i32, i32)> = HashSet::new() ;
    visited.insert((0,0)) ;

    let mut pos_h = (0,0);
    let mut pos_t = (0,0);

    for line in contents.lines() {
        // println!("{line}");
        if line.is_empty() {continue;}
        let input = line.split(" ").collect::<Vec<&str>>() ;

        for _i in 0..input[1].parse().unwrap() {
            let last_h = pos_h ;
            pos_h.0 += decision_map.get(input[0]).unwrap().0;
            pos_h.1 += decision_map.get(input[0]).unwrap().1;
            pos_t = new_knot_pos(pos_t, pos_h) ;
            visited.insert(pos_t);
        }
    }
    
    // println!("{:?}" , visited) ;
    println!("{:?}" , visited.len()) ;

    // part two

    let mut visitedPrime = HashSet::new() ;
    visitedPrime.insert((0,0)) ;

    let num_knots = 10;
    let mut knots : Vec<(i32,i32)> = Vec::with_capacity(num_knots) ;
    for _i in 0..num_knots { knots.push((0,0)) ; }

    for line in contents.lines() {
        // println!("{line}");
        if line.is_empty() {continue;}
        let input = line.split(" ").collect::<Vec<&str>>() ;

        for _i in 0..input[1].parse().unwrap() {
            for j in 0..num_knots {
                if j == 0 {
                    knots[j].0 += decision_map.get(input[0]).unwrap().0;
                    knots[j].1 += decision_map.get(input[0]).unwrap().1;
                }
                else  {
                    knots[j] = new_knot_pos(knots[j], knots[j-1]) ;
                }
                if j == num_knots - 1 { visitedPrime.insert(knots[j]) ; }
            }
        }
    }

    // println!("{:?}" , visited) ;
    println!("{:?}" , visitedPrime.len()) ;

}
