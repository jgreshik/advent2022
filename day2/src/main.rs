//use std::env;
use std::{fs, path::PathBuf, collections::HashMap};

fn play_game_one (left_play: char , right_play: char) -> i32 {

    let trump_map = HashMap::from(
       [('A' , 'C'),
        ('B' , 'A'),
        ('C' , 'B')]);
    let play_map = HashMap::from(
       [('X' , 1),
        ('Y' , 2),
        ('Z' , 3)]);
    let mut points: i32 = 0 ;

    // win
    if trump_map.get(&convert_play(right_play)).unwrap() == &left_play {
        points += 6 ;
    }
    // draw
    else if convert_play(right_play) == left_play {
        points += 3 ;
    }

    return points + play_map.get(&right_play).unwrap() ; 

}

fn play_game_two (left_play: char , right_play: char) -> i32 {

    let trump_map = HashMap::from(
       [('A' , 'C'),
        ('B' , 'A'),
        ('C' , 'B')]);
    let play_map = HashMap::from(
       [('A' , 1),
        ('B' , 2),
        ('C' , 3)]);
    let mut points: i32 = 0 ;

    let real_play: char ;
    // win
    if right_play == 'Z' {
        real_play = trump_map.get(trump_map.get(&left_play).unwrap()).unwrap().clone() ;
        points += 6 ;
    }
    // draw
    else if right_play == 'Y' {
        real_play = left_play ;
        points += 3 ;
    }
    // lose
    else {
        real_play = trump_map.get(&left_play).unwrap().clone() ;
    }

    // println!("{real_play}");
    return points + play_map.get(&real_play).unwrap() ; 

}

fn convert_play (c: char) -> char {
    if c == 'X' { return 'A' ; } 
    else if c ==  'Y' { return 'B' ; }
    return 'C' ;
}

fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

//    println!("{contents}");

    // part one
    let mut points_sum: i32 = 0 ;

    for s in contents.lines() {
        // println!("{s}");
        if s.len()!=3 {continue;}
        let b: Vec<char> = s.chars().collect() ;
        // println!("{:?}",b);
        points_sum += play_game_one(b[0], b[2]) ;
        // println!("{points_sum}");
    }
    
    println!("{points_sum}");

    // part two
    points_sum = 0 ;

    for s in contents.lines() {
        // println!("{s}");
        if s.len()!=3 {continue;}
        let b: Vec<char> = s.chars().collect() ;
        // println!("{:?}",b);
        points_sum += play_game_two(b[0], b[2]) ;
        // println!("{points_sum}");
    }

    println!("{points_sum}");

}
