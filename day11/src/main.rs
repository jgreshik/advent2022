use std::{collections::HashMap, collections::VecDeque};

fn main() {
    // part one
    let mut monkeys = gen_monkeys_hardcode();

    let monkey_id_list: Vec<MonkeyId> = Vec::from([
        MonkeyId::Monkey0,
        MonkeyId::Monkey1,
        MonkeyId::Monkey2,
        MonkeyId::Monkey3,
        MonkeyId::Monkey4,
        MonkeyId::Monkey5,
        MonkeyId::Monkey6,
        MonkeyId::Monkey7,
    ]);

    // let num_rounds = 20;
    let num_rounds = 10000;

    for _i in 0..num_rounds {
        for id in 0..monkey_id_list.len() {
            let mid = monkey_id_list[id];
            monkey_play(mid, &mut monkeys);
        }
    }

    let (mut l1, mut l2): (i64, i64) = (0, 0);
    for _i in monkeys {
        let val: i64 = _i.1.num_touches;
        if val > l1 {
            l2 = l1;
            l1 = val;
        }
        else if val > l2 {
            l2 = val;
        }
    }

    println!("{}", l1 * l2);
}

// this was an exercise in understanding the borrow checker
fn monkey_play(monkey_id: MonkeyId, monkeys: &mut HashMap<MonkeyId, Monkey>) {
    let monkey_list_len = monkeys.get_mut(&monkey_id).unwrap().items.len();

    for _i in 0..monkey_list_len {
        let monkey: &mut Monkey = monkeys.get_mut(&monkey_id).unwrap();
        let items: &mut VecDeque<i64> = &mut monkey.items;
        let test_denom = monkey.test_denom;
        let test_true = monkey.test_true;
        let test_false = monkey.test_false;
        let worry_level = new_item_value(items.pop_front().unwrap(), monkey);
        monkey.num_touches += 1;

        if worry_level % test_denom == 0 {
            monkeys
                .get_mut(&test_true)
                .unwrap()
                .items
                .push_back(worry_level);
        } else {
            monkeys
                .get_mut(&test_false)
                .unwrap()
                .items
                .push_back(worry_level);
        }
    }
}

fn new_item_value(item_value: i64, monkey: &Monkey) -> i64 {
    let val: i64;
    if monkey.op[1] == InputParse::Plus {
        val = item_value
            + if monkey.op[2] == InputParse::Old {
                item_value
            } else {
                monkey.op_int
            };
    } else {
        // mult
        val = item_value
            * if monkey.op[2] == InputParse::Old {
                item_value
            } else {
                monkey.op_int
            };
    }
    // return val / 3;
    return val % 9699690; // commmon primes factor
}

fn gen_monkeys_hardcode() -> HashMap<MonkeyId, Monkey> {
    let mut monkeys: HashMap<MonkeyId, Monkey> = HashMap::new();

    let monkey0 = Monkey {
        id: MonkeyId::Monkey0,
        items: VecDeque::from([72, 64, 51, 57, 93, 97, 68]),
        op: vec![InputParse::Old, InputParse::Times, InputParse::Int],
        op_int: 19,
        test_denom: 17,
        test_true: MonkeyId::Monkey4,
        test_false: MonkeyId::Monkey7,
        num_touches: 0,
    };

    let monkey1 = Monkey {
        id: MonkeyId::Monkey1,
        items: VecDeque::from([62]),
        op: vec![InputParse::Old, InputParse::Times, InputParse::Int],
        op_int: 11,
        test_denom: 3,
        test_true: MonkeyId::Monkey3,
        test_false: MonkeyId::Monkey2,
        num_touches: 0,
    };

    let monkey2 = Monkey {
        id: MonkeyId::Monkey2,
        items: VecDeque::from([57, 94, 69, 79, 72]),
        op: vec![InputParse::Old, InputParse::Plus, InputParse::Int],
        op_int: 6,
        test_denom: 19,
        test_true: MonkeyId::Monkey0,
        test_false: MonkeyId::Monkey4,
        num_touches: 0,
    };

    let monkey3 = Monkey {
        id: MonkeyId::Monkey3,
        items: VecDeque::from([80, 64, 92, 93, 64, 56]),
        op: vec![InputParse::Old, InputParse::Plus, InputParse::Int],
        op_int: 5,
        test_denom: 7,
        test_true: MonkeyId::Monkey2,
        test_false: MonkeyId::Monkey0,
        num_touches: 0,
    };

    let monkey4 = Monkey {
        id: MonkeyId::Monkey4,
        items: VecDeque::from([70, 88, 95, 99, 78, 72, 65, 94]),
        op: vec![InputParse::Old, InputParse::Plus, InputParse::Int],
        op_int: 7,
        test_denom: 2,
        test_true: MonkeyId::Monkey7,
        test_false: MonkeyId::Monkey5,
        num_touches: 0,
    };

    let monkey5 = Monkey {
        id: MonkeyId::Monkey5,
        items: VecDeque::from([57, 95, 81, 61]),
        op: vec![InputParse::Old, InputParse::Times, InputParse::Old],
        op_int: -1,
        test_denom: 5,
        test_true: MonkeyId::Monkey1,
        test_false: MonkeyId::Monkey6,
        num_touches: 0,
    };

    let monkey6 = Monkey {
        id: MonkeyId::Monkey6,
        items: VecDeque::from([79, 99]),
        op: vec![InputParse::Old, InputParse::Plus, InputParse::Int],
        op_int: 2,
        test_denom: 11,
        test_true: MonkeyId::Monkey3,
        test_false: MonkeyId::Monkey1,
        num_touches: 0,
    };

    let monkey7 = Monkey {
        id: MonkeyId::Monkey7,
        items: VecDeque::from([68, 98, 62]),
        op: vec![InputParse::Old, InputParse::Plus, InputParse::Int],
        op_int: 3,
        test_denom: 13,
        test_true: MonkeyId::Monkey5,
        test_false: MonkeyId::Monkey6,
        num_touches: 0,
    };

    monkeys.insert(monkey0.id, monkey0);
    monkeys.insert(monkey1.id, monkey1);
    monkeys.insert(monkey2.id, monkey2);
    monkeys.insert(monkey3.id, monkey3);
    monkeys.insert(monkey4.id, monkey4);
    monkeys.insert(monkey5.id, monkey5);
    monkeys.insert(monkey6.id, monkey6);
    monkeys.insert(monkey7.id, monkey7);

    return monkeys;
}

struct Monkey {
    id: MonkeyId,
    items: VecDeque<i64>,
    op: Vec<InputParse>,
    op_int: i64,
    test_denom: i64,
    test_true: MonkeyId,
    test_false: MonkeyId,
    num_touches: i64,
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
enum InputParse {
    Old,
    Plus,
    Times,
    Int,
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
enum MonkeyId {
    Monkey0,
    Monkey1,
    Monkey2,
    Monkey3,
    Monkey4,
    Monkey5,
    Monkey6,
    Monkey7,
}
