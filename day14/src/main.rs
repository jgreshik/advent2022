use std::{collections::HashSet, fs, path::PathBuf};

fn main() {
    let input = read_input(RunType::Normal);

    let part_one = part_one(&input);

    println!("{:?}", part_one);

    let part_two = part_two(&input);

    println!("{:?}",part_two);
}

fn part_one(input: &Vec<Vec<Point>>) -> usize {

    let (initial_blocked_points_size, maximum_y_val, mut blocked_points) = initialize_blocked_points(input);
    
    loop {
        let sand = sim_falling_sand(&blocked_points, maximum_y_val);
        if sand.y < maximum_y_val {
            blocked_points.insert(sand);
        }
        else {
            break;
        }
    }

    return blocked_points.len() - initial_blocked_points_size;
}

fn part_two(input: &Vec<Vec<Point>>) -> usize {

    let (initial_blocked_points_size, maximum_y_val, mut blocked_points) = initialize_blocked_points(input);
    
    loop {
        let sand = sim_falling_sand_infinite_floor(&blocked_points, maximum_y_val + 2);
        if sand == Point::new(500,0) {
            blocked_points.insert(sand);
            break;
        }
        if sand.y < maximum_y_val + 2 {
            blocked_points.insert(sand);
        }
    }

    return blocked_points.len() - initial_blocked_points_size;
}

// Sand is produced one unit at a time, and the next unit of sand is not produced until the previous unit of sand comes to rest. 
// A unit of sand is large enough to fill one tile of air in your scan.

// A unit of sand always falls down one step if possible. If the tile immediately below is blocked (by rock or sand), 
// the unit of sand attempts to instead move diagonally one step down and to the left. If that tile is blocked, 
// the unit of sand attempts to instead move diagonally one step down and to the right. Sand keeps moving as long as it is able to do so, 
// at each step trying to move down, then down-left, then down-right. If all three possible destinations are blocked, the unit of sand comes 
// to rest and no longer moves, at which point the next unit of sand is created back at the source.

fn sim_falling_sand(blocked_points: &HashSet<Point>, maximum_y_val: i32) -> Point {
    let start = Point::new(500, 0);
    let mut position = start;
    
    while position.y < maximum_y_val {
        // try down
        if !blocked_points.contains(&Point::new(position.x,position.y+1)) {
            position.y += 1;
        } // try down left
        else if !blocked_points.contains(&Point::new(position.x-1,position.y+1)) {
            position.x -= 1;
            position.y += 1;
        } // try down right
        else if !blocked_points.contains(&Point::new(position.x+1,position.y+1)) {
            position.x += 1;
            position.y += 1;
        } // settled so break
        else {
            break;
        }
    }

    return position;
}

fn sim_falling_sand_infinite_floor(blocked_points: &HashSet<Point>, maximum_y_val: i32) -> Point {
    let start = Point::new(500, 0);
    let mut position = start;
    
    while position.y < maximum_y_val {
        // try down
        if !blocked_points.contains(&Point::new(position.x,position.y+1)) {
            position.y += 1;
        } // try down left
        else if !blocked_points.contains(&Point::new(position.x-1,position.y+1)) {
            position.x -= 1;
            position.y += 1;
        } // try down right
        else if !blocked_points.contains(&Point::new(position.x+1,position.y+1)) {
            position.x += 1;
            position.y += 1;
        } // settled so break
        else {
            break;
        }
        
        if position.y + 1 == maximum_y_val {
            break;
        }
    }

    return position;
}

fn initialize_blocked_points(input: &Vec<Vec<Point>>) -> (usize, i32, HashSet<Point>) {
    let mut max_y = std::i32::MIN;
    let mut blocks = HashSet::<Point>::new();
    for i in input {
        for j in 0..(i.len() - 1) {
            let blocked_vec = between_points_inclusive(&i[j], &i[j + 1]);
            for block in blocked_vec {
                blocks.insert(block);
            }
        }
    }
    for block in &blocks {
        max_y = max(max_y, block.y);
    }
    // blocks size, maximum y val, block hash set
    return (blocks.len(), max_y, blocks);
}

fn between_points_inclusive(point_a: &Point, point_b: &Point) -> Vec<Point> {
    let mut vec = Vec::<Point>::new();

    if point_a.x == point_b.x {
        for i in min(point_a.y, point_b.y)..=max(point_a.y, point_b.y) {
            vec.push(Point::new(point_a.x, i));
        }
    } else {
        for i in min(point_a.x, point_b.x)..=max(point_a.x, point_b.x) {
            vec.push(Point::new(i, point_a.y));
        }
    }

    return vec;
}

fn read_input(run_as: RunType) -> Vec<Vec<Point>> {
    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    match run_as {
        RunType::Normal => {
            file_path.push("files/input");
        }
        RunType::Test => {
            file_path.push("files/test");
        }
    }
    let contents: String = fs::read_to_string(file_path).expect("file not open");

    let mut return_vec: Vec<Vec<Point>> = Vec::new();

    for line in contents.lines() {
        if line.is_empty() {
            continue;
        }
        return_vec.push(process_line(line));
    }

    return return_vec;
}

fn process_line(line: &str) -> Vec<Point> {
    return line
        .split(" -> ")
        .collect::<Vec<&str>>()
        .iter()
        .map(|s| s.split(',').collect::<Vec<&str>>())
        .collect::<Vec<Vec<&str>>>()
        .iter()
        .map(|s| {
            s.iter()
                .map(|f| f.parse::<i32>().unwrap())
                .collect::<Vec<i32>>()
        })
        .collect::<Vec<Vec<i32>>>()
        .iter()
        .map(|i| Point::from_vec(i))
        .collect::<Vec<Point>>();
}

fn max<T: std::cmp::PartialOrd>(a: T, b: T) -> T {
    if a > b {
        return a;
    } else {
        return b;
    }
}

fn min<T: std::cmp::PartialOrd>(a: T, b: T) -> T {
    if a < b {
        return a;
    } else {
        return b;
    }
}

enum RunType {
    Test,
    Normal,
}

#[derive(Debug, Hash, Clone, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }
    fn from_vec(data: &Vec<i32>) -> Self {
        Self {
            x: data[0],
            y: data[1],
        }
    }
}
