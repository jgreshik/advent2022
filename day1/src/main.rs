use std::fs;
use std::path::PathBuf;

fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    let contents = fs::read_to_string(file_path)
        .expect("file did not open");

    // println!("{contents}");

    let vec: Vec<&str> = contents.split('\n').collect();
    // println!("{:?}", vec);

    let mut result = vec![];
    let mut sum: i32 = 0;

    for s in vec {
        if ! s.is_empty() {
            let b = s.parse::<i32>().unwrap() ;
            sum += b;
        }
        else {
            result.push(sum);
            sum = 0 ;
        }
    }

    result.sort();

    // println!("{:?}", result);

    // println!("{:?}", result[result.len()-1]);

    let big1 = result[result.len()-1] ;
    let big2 = result[result.len()-2] ;
    let big3 = result[result.len()-3] ;

    println!("{big1}");

    let big_sum = big1+big2+big3;

    println!("{big_sum}");

}
