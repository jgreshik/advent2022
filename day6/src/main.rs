use std::{fs, path::PathBuf, collections::HashSet};

fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

    // println!("{contents}");

    let mut result: String = String::new() ;
    let n = 4 ;
    // let n = 14 ;

    for s in contents.lines() {
        // println!("{s}");
        if s.is_empty() {continue;}
        let char_vec : Vec<char> =  s.chars().collect() ;
        for index in 0..(char_vec.len()-n-1) {
            let mut char_hash : HashSet<char> = HashSet::new() ;
            for i in 0..n { char_hash.insert(char_vec[index + i]); }
            if char_hash.len() == n { 
                for j in 0..n {result.push(char_vec[index+j]);}
                let pretty_index = index + n ;
                println!("Index : {}\nResult: {}\nValue : {}",
                        {&index},
                        {&pretty_index},
                        {&result});
                break;
            }
        }
    }

}
