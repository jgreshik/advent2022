use std::{fs, path::PathBuf};

fn main() {

    let input = read_input(RunType::Normal);

    let part_one = part_one(&input);

    println!("{:?}",part_one);
    
    let part_two = part_two(&input);

    println!("{:?}",part_two);

}

// When comparing two values, the first value is called left and the second value is called
// right. Then:
//
// If both values are integers, the lower integer should come first. If the left integer is
// lower than the right integer, the inputs are in the right order. If the left integer is
// higher than the right integer, the inputs are not in the right order. Otherwise, the
// inputs are the same integer; continue checking the next part of the input.
//
// If both values are lists, compare the first value of each list, then the second value,
// and so on. If the left list runs out of items first, the inputs are in the right order.
// If the right list runs out of items first, the inputs are not in the right order. If the
// lists are the same length and no comparison makes a decision about the order, continue
// checking the next part of the input.
//
// If exactly one value is an integer, convert the integer to a list which contains that
// integer as its only value, then retry the comparison. For example, if comparing [0,0,0]
// and 2, convert the right value to [2] (a list containing 2); the result is then found by
// instead comparing [0,0,0] and [2].

fn compare_node_packets(a_node_vec: &Vec<Node>, b_node_vec: &Vec<Node>) -> NodeCompAction {
    
    let a_len = a_node_vec.len();
    let b_len = b_node_vec.len();

    if a_len != 0 && b_len != 0  {
        
        for i in 0..min(a_len, b_len) {

            let compare_action = compare_nodes(&a_node_vec[i], &b_node_vec[i]);
            
            if compare_action != NodeCompAction::Continue {
                return compare_action;
            }
            
        }

    }

    if a_len > b_len {
        return NodeCompAction::False;
    }
    else {
        return NodeCompAction::True;
    }

}

fn compare_nodes(a_node: &Node, b_node: &Node) -> NodeCompAction {
    
    let a_type = a_node.data_type;
    let b_type = b_node.data_type;
    
    // run through cases
    // 
    
    // both int
    if a_type == NodeType::Integer && b_type == NodeType::Integer {
        return int_comp(a_node.data_integer, b_node.data_integer);
    }

    // both lists
    if a_type == NodeType::StartList && b_type == NodeType::StartList {

        let a_len = a_node.data_list.len();
        let b_len = b_node.data_list.len();

        for i in 0..min(a_len, b_len) {

            let compare_action = compare_nodes(&a_node.data_list[i], &b_node.data_list[i]);
            
            if compare_action != NodeCompAction::Continue {
                return compare_action;
            }
            
        }

        if a_len > b_len {
            return NodeCompAction::False;
        }
        else if a_len < b_len {
            return NodeCompAction::True;
        }

    }
    
    // left int
    if a_type == NodeType::Integer {
        let mut a_list = Node::new(NodeType::StartList, -1);
        a_list.data_list.push(Node::new(NodeType::Integer, a_node.data_integer));
        return compare_nodes(&a_list, b_node);
    }

    // right int
    if b_type == NodeType::Integer {
        let mut b_list = Node::new(NodeType::StartList, -1);
        b_list.data_list.push(Node::new(NodeType::Integer, b_node.data_integer));
        return compare_nodes(a_node, &b_list);
    }
    
    return NodeCompAction::Continue;

}

fn part_one(input: &Vec<Vec<String>>) -> u32 {

    let mut sum = 0;
    let mut index = 1;

    for char_vec_pair in input.chunks(2) {

        if char_vec_pair.is_empty() {
            continue;
        }
        if char_vec_pair.len() != 2 {
            panic!("string_pair length is not 2");
        }
        
        let node0 = str_list_to_node(&char_vec_pair[0].iter().map(|s| &s[..]).collect::<Vec<&str>>(), 
            0 as usize, 
            &mut Vec::from_iter(std::iter::repeat(false).take(char_vec_pair[0].len())), 
            );
        let node1 = str_list_to_node(&char_vec_pair[1].iter().map(|s| &s[..]).collect::<Vec<&str>>(), 
            0 as usize, 
            &mut Vec::from_iter(std::iter::repeat(false).take(char_vec_pair[1].len())), 
            );

        let packet_validity = compare_node_packets(&node0.data_list, &node1.data_list);

        // print_node_pretty(node0);
        // print_node_pretty(node1);
        
        if packet_validity == NodeCompAction::True {
            sum += index;
        }

        index+=1;
    }
        
    return sum;
    
}

fn part_two(input: &Vec<Vec<String>>) -> usize {
    
    let mut node_vec: Vec<Node> = Vec::new();
    
    for char_vec_pair in input.chunks(2) {

        if char_vec_pair.is_empty() {
            continue;
        }
        if char_vec_pair.len() != 2 {
            panic!("string_pair length is not 2");
        }
        
        let node0 = str_list_to_node(&char_vec_pair[0].iter().map(|s| &s[..]).collect::<Vec<&str>>(), 
            0 as usize, 
            &mut Vec::from_iter(std::iter::repeat(false).take(char_vec_pair[0].len())), 
            );
        let node1 = str_list_to_node(&char_vec_pair[1].iter().map(|s| &s[..]).collect::<Vec<&str>>(), 
            0 as usize, 
            &mut Vec::from_iter(std::iter::repeat(false).take(char_vec_pair[1].len())), 
            );
        
        node_vec.push(node0);
        node_vec.push(node1);

    }
    
    // add decoder nodes
    // [[2]]
    let mut decode_node_0 = Node::new(NodeType::StartList, -1);
    let mut decode_node_0_child = Node::new(NodeType::StartList, -1);
    decode_node_0_child.data_list.push(Node::new(NodeType::Integer, 2));
    decode_node_0.data_list.push(decode_node_0_child);
    // [[6]]
    let mut decode_node_1 = Node::new(NodeType::StartList, -1);
    let mut decode_node_1_child = Node::new(NodeType::StartList, -1);
    decode_node_1_child.data_list.push(Node::new(NodeType::Integer, 6));
    decode_node_1.data_list.push(decode_node_1_child);
    
    node_vec.push(decode_node_0.clone());
    node_vec.push(decode_node_1.clone());
    
    node_vec.sort_by(|a, b| 
        if compare_node_packets(&a.data_list, &b.data_list) == NodeCompAction::True 
        {std::cmp::Ordering::Less} else {std::cmp::Ordering::Greater});
        
    // for i in node_vec {
    //     print_node_pretty(i);
    // }
    
    let mut result = 1;
    for i in 0..node_vec.len() {
        if node_vec[i] == decode_node_0 || node_vec[i] == decode_node_1 {
            result *= i + 1;
        }
    }
    
    return result;
}

fn read_input(run_as: RunType) -> Vec<Vec<String>> {
    
    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    match run_as {
        RunType::Normal => {
            file_path.push("files/input");
        }
        RunType::Test => {
            file_path.push("files/test");
        }
    }
    let contents: String = fs::read_to_string(file_path)
        .expect("file not open");

    let mut s_vec: Vec<Vec<String>> = Vec::new();

    for line in contents.lines() {
        if line.is_empty() {continue;}
        let sline = process_line(line);
        s_vec.push(sline);
    }
    
    return s_vec;

}

fn str_list_to_node(string_list: &Vec<&str>, index: usize, visited: &mut Vec<bool>) -> Node { 
    
    if index >= string_list.len() || visited[index] {
        return Node::new(NodeType::None, -1);
    }
    
    visited[index] = true;
    
    let val = string_list[index];
    
    let mut node;
    
    if val == "[" { // [
        node = Node::new(NodeType::StartList, -1);
        
        let mut child;
        let mut i = 1;
        
        let mut stack_count = 1;
        
        while stack_count > 0 {
            
            child = str_list_to_node(string_list, index+i, visited);
            i += 1;

            let child_type = child.data_type;

            if child_type != NodeType::None && child_type != NodeType::EndList {
                node.data_list.push(child); // consume the child
            }

            else if child_type == NodeType::EndList {
                stack_count -= 1;
            }

        }
    }
    else if val == "]" { // ]
        node = Node::new(NodeType::EndList, -1);
    }
    else { // integer
        node = Node::new(NodeType::Integer, 
            val.to_string().parse::<i32>().unwrap());
    }

    return node;
}

fn process_line(line: &str) -> Vec<String> {
    let mut s_vec: Vec<String> = Vec::new();
    let mut i = 0;
    let chars = line.chars().collect::<Vec<char>>();
    while i < line.len() {
        let char = chars[i];
        match char {
            ',' => (),
            '[' => s_vec.push(char.to_string()),
            ']' => s_vec.push(char.to_string()),
            _ => {
                if chars[i-1].is_numeric() {
                    s_vec.last_mut().unwrap().push_str(char.to_string().as_str());
                }
                else {
                    s_vec.push(char.to_string());
                }
            },
        }
        i += 1;
    }
    return s_vec;
}

fn min<T: std::cmp::PartialOrd>(a: T, b: T) -> T {
    if a < b {
        return a;
    }
    else {
        return b;
    }
}

// a left, b right
fn int_comp<T: std::cmp::PartialOrd>(a: T, b: T) -> NodeCompAction {
    if a > b {
        return NodeCompAction::False;
    }
    else if a < b {
        return NodeCompAction::True;
    }
    else {
        return NodeCompAction::Continue;
    }
}

fn print_node_pretty(node: Node) {
    print_node(node);
    println!();
}

fn print_node(node: Node) {
    if node.data_type == NodeType::Integer {
        print!("{:?}, ",node.data_integer);
    }
    else if node.data_type == NodeType::EndList {
        print!("None, ");
    }
    else if node.data_type == NodeType::None {
        print!("None, ");
    }
    else if node.data_type == NodeType::StartList {
        print!("[");
        for n in node.data_list {
            print_node(n);
        }
        print!("]");
    }
}

#[derive(PartialEq, Debug, Copy, Clone)]
enum NodeType {
    Integer,
    StartList,
    EndList,
    None,
}

#[derive(PartialEq, Debug)]
enum NodeCompAction {
    Continue,
    True,
    False,
}

enum RunType {
    Test,
    Normal,
}

#[derive(Debug, Clone, PartialEq)]
struct Node
{
    data_type: NodeType,
    data_integer: i32,
    data_list: Vec<Node>,
}

impl Node
{
    fn new(data_type: NodeType, data_integer: i32) -> Self {
        Self {
            data_type,
            data_integer,
            data_list: Vec::new(),
        }
    }
}