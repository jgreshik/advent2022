use std::{fs, path::PathBuf};

fn main() {
    
    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

    // println!("{contents}");

    // part one 

    let mut clock = 1 ;
    let mut x : i32 = 1 ;
    
    let mut sum = 0 ;
    
    for line in contents.lines() {
        // println!("{line}");
        let input = line.split(" ").collect::<Vec<&str>>() ;
        if line.is_empty() {continue;}
        let val : i32 ;
        if input.len() > 1 {
            val = input[1].parse().unwrap() ;
            clock+=2;
            x+=val;
            if clock-1 == 20 || clock-1 == 60 || clock-1 == 100 || clock-1 == 140 || clock-1 == 180 || clock-1 == 220 {sum += (clock -1) * (x - val) ;}
            // println!("{} : {} : {}" , clock-1 , (x - val) , (clock - 1) * (x - val)) ;
        }
        else {
            clock+=1;
        }
        if clock == 20 || clock == 60 || clock == 100 || clock == 140 || clock == 180 || clock == 220 {sum += clock * x ;}
        // println!("{} : {} : {}" , clock , x , clock * x) ;
    }
    
    println!("{:?}",sum);
    
    let mut screen : Vec<Vec<char>> = Vec::new() ;
    let (n , m) = (6 , 40) ;
    
    for i in 0..n {
        screen.push(Vec::new()) ;
        for _j in 0..m {
            screen[i].push('.') ;
        }
    }

    clock = 1 ;
    x = 1 ;

    for line in contents.lines() {
        // println!("{line}");
        let input = line.split(" ").collect::<Vec<&str>>() ;
        if line.is_empty() {continue;}
        let val : i32 ;
        let mut uclock : usize ;

        uclock = (clock-1).try_into().unwrap() ;
        if (clock % (m as i32) - 1 - x % (m as i32)).abs() <= 1 {screen[uclock / m][uclock % m] = '#';}

        if input.len() > 1 {
            val = input[1].parse().unwrap() ;

            clock+=1;

            uclock = (clock-1).try_into().unwrap() ;
            if (clock % (m as i32) - 1 - x % (m as i32)).abs() <= 1 {screen[uclock / m][uclock % m] = '#';}

            clock+=1;

            x+=val;

            // println!("{} : {} : {}" , clock-1 , (x - val) , (clock - 1) * (x - val)) ;

        }
        else {
            clock+=1;
        }
        // println!("{} : {} : {}" , clock , x , clock * x) ;
    }

    for i in screen {
        println!("") ;
        for j in i {
            print!("{j}") ;
        }
    }
    println!("") ;

}
