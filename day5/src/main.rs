use std::{fs, path::PathBuf};

// [G]                 [D] [R]
// [W]         [V]     [C] [T] [M]
// [L]         [P] [Z] [Q] [F] [V]
// [J]         [S] [D] [J] [M] [T] [V]
// [B]     [M] [H] [L] [Z] [J] [B] [S]
// [R] [C] [T] [C] [T] [R] [D] [R] [D]
// [T] [W] [Z] [T] [P] [B] [B] [H] [P]
// [D] [S] [R] [D] [G] [F] [S] [L] [Q]
//  1   2   3   4   5   6   7   8   9
// G W L J B R T D
// C W S
// M T Z R
// V P S H C T D
// Z D L T P G
// D C Q J Z R B F
// R T F M J D B S
// M V T B R H L
// V S D P Q
// 
// input originally looked like
// move 1 from 3 to 5
// move 5 from 5 to 4
// move 6 from 7 to 3
// move 6 from 1 to 3
// 
// now is 
// 1 3 5
// 5 5 4
// 6 7 3
// 6 1 3
// 

fn print_stacks (stacks: &Vec<Vec<char>>) {
    let mut it: i32 = 1 ;
    for stack in stacks {
        let mut s : String = String::new() ;
        s.push_str(&it.to_string());
        s.push_str(&" : ");
        for c in stack {
            s.push(c.clone());
        }
        it += 1 ;
        println!("{s}");
    }
    println!("");
}

fn create_stacks () -> Vec<Vec<char>> {

    // HARDCODER DO
    let create_stacks : Vec<Vec<char>> = Vec::from([
        vec!['G' , 'W' , 'L' , 'J' , 'B' , 'R' , 'T' , 'D' ],       // 0
        vec!['C' , 'W' , 'S' ],                                     // 1
        vec!['M' , 'T' , 'Z' , 'R' ],                               // 2
        vec!['V' , 'P' , 'S' , 'H' , 'C' , 'T' , 'D' ],             // 3
        vec!['Z' , 'D' , 'L' , 'T' , 'P' , 'G' ],                   // 4
        vec!['D' , 'C' , 'Q' , 'J' , 'Z' , 'R' , 'B' , 'F' ],       // 5
        vec!['R' , 'T' , 'F' , 'M' , 'J' , 'D' , 'B' , 'S' ],       // 6
        vec!['M' , 'V' , 'T' , 'B' , 'R' , 'H' , 'L' ],             // 7 
        vec!['V' , 'S' , 'D' , 'P' , 'Q' ],                         // 8
    ]) ;

    let mut stacks: Vec<Vec<char>> = Vec::new() ;

    // i can explain
    let mut stack_it: i32 = 0 ;
    for v in create_stacks {
        stacks.push(Vec::new());
        for c in v {
           stacks[stack_it as usize].push(c);
        }
        stacks[stack_it as usize].reverse();
        stack_it += 1;
    }
    
    return stacks;

}

fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

//    println!("{contents}");

    // part one 
    let mut stacks = create_stacks() ;

    // print_stacks(&stacks) ;
    for line in contents.lines() {
        if line.is_empty() { continue ; }
        // instruction 
        // move 0 from 1 to 2
        let instruction: Vec<i32> = 
              line.split(' ').map(|s| s.trim())
              .filter(|s| !s.is_empty())
              .map(|s| s.parse().unwrap())
              .collect();
        // println!("{:?}",instruction);
        for i in 0..instruction[0] {
            if stacks[(instruction[1]-1) as usize].is_empty() {continue;}
            let popped: char = stacks[(instruction[1]-1) as usize].pop().unwrap() ;
            stacks[(instruction[2]-1) as usize].push(popped) ;
        }
        // print_stacks(&stacks) ;
    }

    let mut result: String = String::new() ;
    for i in 0..9 {
        if stacks[i as usize].is_empty() {result.push(' ') ; continue ;}
        let char_pop = stacks[i as usize].pop().unwrap() ;
        result.push(char_pop) ;
    }

    println!("{result}") ;
    
    // part two
    let mut stacks_9001 = create_stacks() ;
    
    // print_stacks(&stacks_9001) ;
    for line in contents.lines() {
        if line.is_empty() { continue ; }
        // instruction 
        // move 0 from 1 to 2
        let instruction: Vec<i32> = 
              line.split(' ').map(|s| s.trim())
              .filter(|s| !s.is_empty())
              .map(|s| s.parse().unwrap())
              .collect();
        // println!("{:?}",instruction);
        let mut popped: Vec<char> = Vec::new();
        for i in 0..instruction[0] {
            if stacks_9001[(instruction[1]-1) as usize].is_empty() {continue;}
            popped.push(stacks_9001[(instruction[1]-1) as usize].pop().unwrap()) ;
        }
        popped.reverse();
        for c in popped {
            stacks_9001[(instruction[2]-1) as usize].push(c) ;
        }
        // print_stacks(&stacks_9001) ;
    }
    
    let mut result: String = String::new() ;
    for i in 0..9 {
        if stacks_9001[i as usize].is_empty() {result.push(' ') ; continue ;}
        let char_pop = stacks_9001[i as usize].pop().unwrap() ;
        result.push(char_pop) ;
    }

    println!("{result}") ;

}
