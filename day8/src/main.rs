use std::{fs, path::PathBuf};

fn calc_distance (forest : &Vec<Vec<i32>>, n : usize , m : usize , i : usize , j : usize) -> i32 {
    if i >= (n-1) || j >= (m-1) || i <= 0 || j <= 0 {return 0 ;}
    let (mut index , mut down , mut up , mut left , mut right ) = (0, 0, 0, 0, 0) ;
    index = i as i32 ; while index < (n-1) as i32  { down  +=1 ; index+=1 ; if forest[index as usize][j] >= forest[i][j] { break; } } ;
    index = i as i32 ; while index > 0             { up    +=1 ; index-=1 ; if forest[index as usize][j] >= forest[i][j] { break; } } ;    
    index = j as i32 ; while index > 0             { left  +=1 ; index-=1 ; if forest[i][index as usize] >= forest[i][j] { break; } } ;
    index = j as i32 ; while index < (m-1) as i32  { right +=1 ; index+=1 ; if forest[i][index as usize] >= forest[i][j] { break; } } ;
    return down * up * left * right ; 
}

fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

    // println!("{contents}");

    let mut forest  : Vec<Vec<i32>> = Vec::new() ;

    for line in contents.lines() {
        // println!("{line}");
        if line.is_empty() {continue;}
        forest.push(
            line.split("")
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| s.parse().unwrap())
            .collect());
    }
    
    // part 1 

    let n = forest.len()    ;
    let m = forest[0].len() ;

    let mut row_max : Vec<i32> = vec![0;m] ;
    let mut col_max : Vec<i32> = vec![0;n] ;

    let mut hidden  : Vec<Vec<i32>> = vec![vec![0;m];n] ;

    // right down
    for i in 0..(n-1) {
        for j in 0..(m-1) {
            if forest[i][j] <= row_max[i] && forest[i][j] <= col_max[j] {
                if i != 0 && j != 0 { hidden[i][j] += 1 ; }
            } 
            if forest[i][j] > row_max[i] { row_max[i] = forest[i][j] ; }
            if forest[i][j] > col_max[j] { col_max[j] = forest[i][j] ; }
        }
    }

    for i in 0..row_max.len() { row_max[i] = 0 ; }
    for i in 0..col_max.len() { col_max[i] = 0 ; }

    // left up
    for i in (1..n).rev() {
        for j in (1..m).rev() {
            if forest[i][j] <= row_max[i] && forest[i][j] <= col_max[j] {
                if i != 0 && j != 0 { hidden[i][j] += 1 ; }
            } 
            if forest[i][j] > row_max[i] { row_max[i] = forest[i][j] ; }
            if forest[i][j] > col_max[j] { col_max[j] = forest[i][j] ; }
        }
    }
    
    let hidden_trees: i32 = hidden.iter().flatten()
        .fold(0, |num_twos, element| num_twos + (element == &2 as &i32) as i32 ) ;
    
    let result1 = m as i32 * n as i32 - hidden_trees ;
    println!("{result1}") ;
    
    // part 2

    let mut scenic_calcs  : Vec<i32> = Vec::new() ;
    for i in 0..(n-1) {
        for j in 0..(m-1) {
            scenic_calcs.push(calc_distance(&forest, n, m, i, j)) ;
        }
    }
    
    let result2 = scenic_calcs.iter().max().unwrap() ;
    println!("{result2}") ;

}
