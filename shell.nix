{ pkgs ? import <nixpkgs> {}}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    rustc
    cargo
    rustfmt
    rust-analyzer
    clippy
    neovim
  ];

  RUST_BACKTRACE = 1;
  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";

  shellHook = ''
    # codium extensions
    codium --install-extension asvetliakov.vscode-neovim
    codium --install-extension rust-lang.rust-analyzer
    codium --install-extension vadimcn.vscode-lldb
  '';

# allowing remapped jf escape sequence in vscodium
#// Place your key bindings in this file to override the defaults
#[
#    {
#        "command": "vscode-neovim.compositeEscape1",
#        "key": "j",
#        "when": "neovim.mode == insert && editorTextFocus",
#        "args": "j"
#    },
#    {
#        "command": "vscode-neovim.compositeEscape1",
#        "key": "f",
#        "when": "neovim.mode == insert && editorTextFocus",
#        "args": "f"
#    }
#]

}
