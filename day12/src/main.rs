use std::{fs, path::PathBuf, collections::{HashMap, VecDeque}};

fn main() {

    let string_input = read_input(RunType::Normal);
    let (start, end, elevation_map) = 
        alpha_vec_to_int_vec(&string_input);

    let mut graph = build_graph(elevation_map);
    
    let mut shortest_path = do_bfs(start, end, &mut graph);
    
    println!("{:?}",shortest_path);

    shortest_path = min_a_start(&string_input, &mut graph, end);

    println!("{:?}",shortest_path);

}

fn min_a_start(string_vec: &Vec<String>, graph: &mut HashMap<(i32,i32), Node>, end: (i32,i32)) -> i32 {

    let mut shortest_path = i32::MAX;
    
    let n = string_vec.len();
    let m = string_vec[0].len();
    
    for i in 0..n {
        for j in 0..m {
            if string_vec[i].chars().nth(j).unwrap() == 'a' {
                let path = do_bfs((i as i32, j as i32), end, graph);
                shortest_path = if path < shortest_path {path} else {shortest_path}
            }
        }
    }

    return shortest_path;
}

fn do_bfs(start: (i32,i32), end: (i32,i32), graph: &mut HashMap<(i32,i32), Node>) -> i32 {
    
    let mut queue: VecDeque<(i32,i32)> = VecDeque::new();
    
    let mut finish_flag = false;
    
    // println!("{:?}",start);
    // print_graph(graph);

    graph.get_mut(&start).unwrap().visited = true;
    queue.push_back(start);
    
    while !queue.is_empty() {
        let node = queue.pop_front().unwrap();
        let current_value = graph.get(&node).unwrap().value;
        if node == end {
            finish_flag = true;
            break;
        }
        let neighbor_vec = &graph.get(&node).unwrap().neighbors.clone();
        for neighbor in neighbor_vec {
                let neighbor_node = graph.get_mut(&neighbor).unwrap();
                if neighbor_node.visited == false {
                    neighbor_node.visited = true;
                    neighbor_node.value += current_value + 1;
                    queue.push_back(*neighbor);
                }
           }
    }
    
    let shortest_path = if finish_flag {graph.get(&end).unwrap().value.clone()} else {i32::MAX};

    // reset graph nodes
    let indices = graph.keys().cloned().collect::<Vec<(i32,i32)>>(); 
    for i in indices {
        graph.get_mut(&i).unwrap().visited = false; 
        graph.get_mut(&i).unwrap().value = 0; 
    }
    
    return shortest_path;
} 

fn build_graph(input: Vec<Vec<i32>>) -> HashMap<(i32,i32), Node> {
    let mut graph: HashMap<(i32,i32), Node> = HashMap::new();
    let (n,m) = (input.len(), input[0].len());
    let check_vec: Vec<(i32,i32)> = Vec::from([(1,0),(-1,0),(0,1),(0,-1)]);
    for row in 0..n {
        for col in 0..m {
            let mut node = Node {
                neighbors: Vec::new(),
                visited: false,
                value: 0,
            };
            for k in &check_vec {
                let i = row as i32 + k.0;
                let j = col as i32 + k.1;
                if i >=0 && j>=0 && j < m as i32 && i<n as i32 {
                    if input[i as usize][j as usize]<=input[row][col]+1 {
                        node.neighbors.push((i,j));
                    }
                }
            }
            graph.insert((row as i32,col as i32), node);
        }
    }
    return graph;
}

fn read_input(run_as: RunType) -> Vec<String> {
    
    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    match run_as {
        RunType::Normal => {
            file_path.push("files/input");
        }
        RunType::Test => {
            file_path.push("files/test");
        }
    }
    let contents = fs::read_to_string(file_path)
        .expect("file not open");

    let mut str_vec: Vec<String> = Vec::new();

    for line in contents.lines() {
        if line.is_empty() {continue;}
        str_vec.push(line.to_string());
    }
    
    return str_vec;

}

fn alpha_vec_to_int_vec(str_vec: &Vec<String>) -> ((i32, i32), (i32, i32), Vec<Vec<i32>>) {

    let alpha_low = (b'a'..=b'z')                               // Start as u8
        .filter_map(|c| {
            let c = c as char;                             // Convert to char
            if c.is_alphabetic() { Some(c) } else { None } // Filter only alphabetic chars
        })          
        .collect::<Vec<_>>();
    
    let mut char_to_int_map: HashMap<char, i32> = HashMap::new() ;
     
    let mut it: i32 = 1 ;
    for c in alpha_low {
        char_to_int_map.insert(c, it) ;
        it+=1;
    }
    
    let mut int_vec: Vec<Vec<i32>> = Vec::new();
    let mut index_row = 0;
    let mut index_col;
    
    let mut start = (0,0);
    let mut end = (0,0);
    
    for string in str_vec {
        int_vec.push(Vec::new());
        index_col = 0;
        for char in string.chars() {
            let mut c = char;
            if char == 'S' {
                c = 'a';
                start = (index_row as i32, index_col as i32);
            }
            if char == 'E' {
                c = 'z';
                end = (index_row as i32, index_col as i32);
            }
            let val = char_to_int_map.get(&c).unwrap();
            int_vec[index_row].push(*val);
            index_col+=1;
        }
        index_row+=1;
    }
    
    return (start, end, int_vec);

}

fn print_graph(graph: &mut HashMap<(i32,i32), Node>) {
    for val in graph {
        println!("{:?} {:?}",val.0, val.1.neighbors);
    }
}
    
fn print_elevation_map(elevation_map: Vec<Vec<i32>>) {
    for i in &elevation_map {
        println!("") ;
        for j in i {
            print!("{j:3}") ;
        }
    }
    println!("") ;
}

enum RunType {
    Test,
    Normal,
}

struct Node {
    neighbors: Vec<(i32,i32)>,
    visited: bool,
    value: i32,
}