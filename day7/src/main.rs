use std::{fs, path::PathBuf, collections::HashMap};

fn main() {

    let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    file_path.push("files/input");
    // file_path.push("files/test");
    let contents : String = fs::read_to_string(file_path).expect("file not open");
    
    // println!("{contents}");

    // part 1 
    
    let mut curr_dir : String = "".to_string() ;
    let mut fsys : HashMap<String,i32> = HashMap::new() ;

    for line in contents.lines() {
        // println!("{line}");
        let pline : Vec<&str> = line.split(' ').collect() ;
        if line.is_empty() {continue;}
        if pline[0] == "$" {
            if pline[1] == "cd" {
                if pline[2] == ".." {
                    let mut pcurr_dir : Vec<&str> = curr_dir.split_inclusive('/').collect() ;
                    pcurr_dir.pop() ;
                    curr_dir = pcurr_dir.join("") ;
                }
                else {
                    // update curr_dir
                    curr_dir.push_str(pline[2]);
                    if ! curr_dir.ends_with("/") {curr_dir.push_str("/");}
                    // println!("{curr_dir}");
                    // check if needs to be added to map
                    // add to map 
                    if ! fsys.contains_key(&curr_dir.clone()) {
                        fsys.insert(curr_dir.clone(), 0) ;
                    }
                }
            }
        }
        else if pline[0] != "dir" {
            // add file size to all dirs in path
            let mut pcurr_dir : Vec<&str> = curr_dir.split_inclusive('/').collect() ;
            while ! pcurr_dir.is_empty() {
                let key = pcurr_dir.clone().join("") ;
                // println!("{key}") ;
                let size : i32 = pline[0].parse().unwrap() ;
                *fsys.get_mut(&*key).unwrap() += size ;
                pcurr_dir.pop() ;
            }
        }
    }

    let mut sum : i32 = 0 ;
    let mut key_vals : Vec<_> = fsys.iter().collect() ;
    key_vals.sort_by(|a,b| a.1.cmp(b.1)) ;

    for i in 0..(key_vals.len()-1) {
        if key_vals[i].1 <= &100000 {
            // println!("{} : {}" , i.0 , i.1);
            sum += key_vals[i].1 ;
        }
    }
    
    println!("{sum}") ;
    
    // part 2 

    let total_disk : i32        = 70000000 ;
    let unused_disk_goal : i32  = 30000000 ;
    let disk_goal : i32         = unused_disk_goal - (total_disk - key_vals.last().unwrap().1) ;

    println!("Disk goal : {}", disk_goal) ;

    for i in key_vals {
        if i.1 > &disk_goal {
            println!("{:?}", i.1) ;
            break ;
        }
    }

}
